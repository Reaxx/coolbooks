﻿function CharCounter(id, outputId) {
    var len = document.getElementById(id).value.length;
    var maxLen = document.getElementById(id).maxLength;
    if (maxLen != -1) {
        document.getElementById(outputId).textContent = len + " of " + maxLen + " characters used";
    }
    else {
        document.getElementById(outputId).textContent = len + " characters used";
    }
}


function UpdateNumber(target, invalue) {
    document.getElementById(target).value = parseInt(invalue);
}