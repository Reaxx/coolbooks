﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoolBooks.Controllers
{
    public class RatingsController : Controller
    {
		private CoolBooksEntities db = new CoolBooksEntities();
		// GET: Ratings
		public ActionResult MostRated()
        {
			var Model = db.Books.ToList();
			//Model = Model.OrderByDescending(x => x.Ratings).Take(5).ToList();

			return View("_BookList",Model);
        }
		public ActionResult HighestRated()
		{
			var Model = db.Books.ToList();
			Model = Model.OrderByDescending(x => x.Ratings).Take(5).ToList();

			return View("_BookList", Model);
		}
		//public ActionResult LatestRated()
		//{
		//	var Model = db.Reviews.OrderByDescending(x => x.Created).Distinct().Take(5).ToList();
		//}
	}
}