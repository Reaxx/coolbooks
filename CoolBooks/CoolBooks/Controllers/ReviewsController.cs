﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CoolBooks;
using Microsoft.AspNet.Identity;
using CoolBooks.Classes;

namespace CoolBooks.Controllers
{
    public class ReviewsController : Controller
    {
        private CoolBooksEntities db = new CoolBooksEntities();

		// GET: Reviews
		[Authorize(Roles = "Admin")]
		public ActionResult Index()
        {
            var reviews = db.Reviews.Include(r => r.Book).Include(r => r.AspNetUser);
            return View(reviews.ToList());
        }

        // GET: Reviews/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

		// GET: Reviews/Create
		[Authorize]
		public ActionResult Create(int bookid)
        {
			var book = (from r in db.Books
						where r.Id ==bookid
						select r).First();
			var _review = (new Review { BookId = book.Id,
							UserId = User.Identity.GetUserId(),
							Created=DateTime.Now
			});
			ViewBag.BookTitle = book.Title;
			ViewBag.BookPath = book.ImagePath;

			return View(_review);
        }

        // POST: Reviews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[Authorize]
		[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BookId,UserId,Title,Text,Rating,Created,IsDeleted")] Review review) //id
        {
			review.Created = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Reviews.Add(review);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BookId = new SelectList(db.Books, "Id", "UserId", review.BookId);
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", review.UserId);
            return View(review);
        }

        // GET: Reviews/Edit/5
		[AuthorizeCreatorAndAdmin]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookId = new SelectList(db.Books, "Id", "Title", review.BookId);
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", review.UserId);
            return View(review);
        }

        // POST: Reviews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[AuthorizeCreatorAndAdmin]
		[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BookId,UserId,Title,Text,Rating,Created,IsDeleted")] Review review)
        {
            if (ModelState.IsValid)
            {
				if(!User.IsInRole("Admin"))
				{
					review.UserId = User.Identity.GetUserId();
				}
                db.Entry(review).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookId = new SelectList(db.Books, "Id", "UserId", review.BookId);
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", review.UserId);
            return View(review);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
