﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoolBooks.Controllers
{


	public class HomeController : Controller
	{
		private CoolBooksEntities db = new CoolBooksEntities();

		

		// GET: BookImageRow
		[ChildActionOnly]
		public ActionResult PWBooksbyGenre(int nr)
		{
			var genres = db.Genres.Where(g => !g.IsDeleted).ToList();

			Dictionary<string, List<Book>> model = new Dictionary<string, List<Book>>();

			foreach (var item in genres)
			{
				if (db.Books.Include("Genre").Count(b => b.Genre.Name == item.Name && !b.IsDeleted) != 0)
				{
					model.Add(item.Name,
						new List<Book>(db.Books
						.OrderByDescending(b => b.Created)
						.Where(b => !b.IsDeleted && b.Genre.Name == item.Name)
						.Take(nr)
						.ToList()
						));
				}
			}
			
			return PartialView(model);
		}


		public ActionResult Index()
		{
			Random rand = new Random();
			var count = (from r in db.Books
						 select r.Id).Count();
			
			//var model = (from r in db.Books
			//			 select r)
			//			.Skip(rand.Next(1, count))
			//			.Take(1).First();
			var model = db.Books.OrderBy(x => x.Id).Skip(rand.Next(1,count)).Take(1).First();
		

			return View(model);
		}

        public ActionResult Contact()
        {
            return View();
        }


         
    }
}