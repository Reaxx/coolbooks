﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CoolBooks;

namespace CoolBooks.Controllers
{
    public class SearchController : Controller
    {
        private CoolBooksEntities db = new CoolBooksEntities();

        // GET: Search
        public ActionResult Index(string search)
        {
            var title = db.Books.Where(x => x.Title.Contains(search) && !(x.IsDeleted)).Include(b => b.Author).ToList();
            var author = db.Books.Where(x => x.Author.LastName.Contains(search) && !(x.IsDeleted)).Include(b => b.Author).ToList();

            var books = title.Union(author);
            return View(books);
        }


        // GET: AdvancedSearch
        public ActionResult AdvancedSearch(string search)
        {
            var titleSearch = HttpContext.Request.Params.Get("Booktitle");
            var authorSearch = HttpContext.Request.Params.Get("Author");
            var genreSearch = HttpContext.Request.Params.Get("Genre");
            var isbnSearch = HttpContext.Request.Params.Get("ISBN");

            ViewBag.Booktitle = titleSearch;
            ViewBag.Author = authorSearch;
            ViewBag.Genre = genreSearch;
            ViewBag.ISBN = isbnSearch;

            var searchResult = db.Books.Where
                (x => x.Title.Contains(titleSearch) && 
                x.Author.LastName.Contains(authorSearch) && 
                x.Genre.Name.Contains(genreSearch) && 
                x.ISBN.Contains(isbnSearch) && !(x.IsDeleted)).Include(b => b.Author).ToList();

            return View("Index",searchResult);
        }
		
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
