﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CoolBooks;
using CoolBooks.Models;

namespace CoolBooks.Controllers
{
	public class HighRankedController : Controller
	{
		private CoolBooksEntities db = new CoolBooksEntities();
		protected class dictval
		{
			public dictval(int i,int start)
			{
				id = i;
				total = start;
				nr = 1;
			}
			public void add(int ad)
			{
				total += ad;
				nr++;
			}
			int id;
			int total = 0;
			int nr = 0;
			public int ID { get { return id; } set {id=value; } }
			public int Total { get { return total; } set { total = value; } }
			public int NR { get { return nr; } set { nr = value; } }
		}


		public ActionResult MostRatedView()
		{
			var model = db.Books.ToList().OrderByDescending(x => x.Ratings).Take(5);
			

			return PartialView(model);
		}

		// GET: HighRanked
		public ActionResult HighRankedView()
		{

			var model = db.Books.ToList().OrderByDescending(x => x.Rating).Take(5);

			return PartialView(model);
		}
		public ActionResult LatestRanked()
		{

			var model = db.Reviews.Where(x => !x.IsDeleted).OrderByDescending(x => x.Created).Take(5).ToList();

			return PartialView(model);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
