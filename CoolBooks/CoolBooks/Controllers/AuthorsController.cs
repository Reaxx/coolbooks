using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CoolBooks;

namespace CoolBooks.Controllers
{
    public class AuthorsController : Controller
    {
        private CoolBooksEntities db = new CoolBooksEntities();

		[ChildActionOnly]
		public ActionResult ShowBooks(int Id)
		{
			var model = new List<Book>();
			if (User.IsInRole("Admin"))
			{
				model = db.Books.Where(r => r.AuthorId == Id).OrderByDescending(x => x.Created).ToList();
			}
			else
			{
				model = db.Books.Where(r => r.AuthorId == Id && !r.IsDeleted).OrderByDescending(x => x.Created).ToList();
			}
			if (model.Count() == 0)
			{
				return Content("No books yet");
			}
			else
			{
				return PartialView(model);
			}
		}

		// GET: Authors
		[Authorize(Roles = "Admin")]
		public ActionResult Index()
        {
            return View(db.Authors.ToList());
        }

        // GET: Authors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Author author = db.Authors.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }

		// GET: Authors/Create
		[Authorize(Roles = "Admin")]
		public ActionResult Create()
        {
            return View();
        }

        // POST: Authors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[Authorize(Roles = "Admin")]
		[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Description,Created,IsDeleted")] Author author)
        {
            if (ModelState.IsValid)
            {
                db.Authors.Add(author);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(author);
        }

		// GET: Authors/Edit/5
		[Authorize(Roles = "Admin")]
		public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Author author = db.Authors.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }

        // POST: Authors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[Authorize(Roles = "Admin")]
		[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Description,Created,IsDeleted")] Author author)
        {
            if (ModelState.IsValid)
            {
                db.Entry(author).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(author);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
