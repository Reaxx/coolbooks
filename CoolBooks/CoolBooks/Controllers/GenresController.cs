﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CoolBooks;

namespace CoolBooks.Controllers
{
    public class GenresController : Controller
    {
        private CoolBooksEntities db = new CoolBooksEntities();

		[ChildActionOnly]
		public ActionResult ShowBooks(int Id)
		{
			var model = new List<Book>();
			if (User.IsInRole("Admin"))
			{
				model = db.Books.Where(r => r.AuthorId == Id).ToList();
			}
			else
			{
				model = db.Books.Where(r => r.AuthorId == Id && !r.IsDeleted).ToList();
			}
			if (model.Count() == 0)
			{
				return Content("No books yet");
			}
			else
			{
				return PartialView(model);
			}
		}

		// GET: Genres
		[Authorize(Roles = "Admin")]
		public ActionResult Index()
        {
            return View(db.Genres.ToList());
        }

        // GET: Genres/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Genre genre = db.Genres.Find(id);
            if (genre == null)
            {
                return HttpNotFound();
            }
            return View(genre);
        }

		// GET: Genres/Create
		[Authorize(Roles = "Admin")]
		public ActionResult Create()
        {
            return View();
        }

        // POST: Genres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Created,IsDeleted")] Genre genre)
        {
            if (ModelState.IsValid)
            {
				genre.Created = DateTime.Now;
                db.Genres.Add(genre);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(genre);
        }

		// GET: Genres/Edit/5
		[Authorize(Roles = "Admin")]
		public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Genre genre = db.Genres.Find(id);
            if (genre == null)
            {
                return HttpNotFound();
            }
            return View(genre);
        }

        // POST: Genres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[Authorize(Roles = "Admin")]
		[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Created,IsDeleted")] Genre genre)
        {
            if (ModelState.IsValid)
            {
                db.Entry(genre).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(genre);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
