﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CoolBooks;
using Microsoft.AspNet.Identity;
using CoolBooks.Classes;

namespace CoolBooks.Controllers
{
    public class BooksController : Controller
    {
        private CoolBooksEntities db = new CoolBooksEntities();

		public JsonResult IsISBNExists(string isbn, int Id = 0)
		{
			Book book = db.Books.FirstOrDefault(b => b.ISBN == isbn);
			if (book == null || book.Id == Id)
			{
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		[ChildActionOnly]
		public ActionResult ShowReviews(int Id)
		{
			var model = new List<Review>();
			if(User.IsInRole("Admin"))
			{
				model = db.Reviews.Where(r => r.BookId == Id).OrderByDescending(x => x.Created).ToList();
			}
			else
			{
				var userId = User.Identity.GetUserId();
				model = db.Reviews.Where(r => r.BookId == Id && (!r.IsDeleted || r.UserId == userId)).OrderByDescending(x => x.Created).ToList();
			}
			if (model.Count() == 0)
			{
				return Content("No reviews yet");
			}
			else
			{
				return PartialView(model);
			}
		}

		// GET: Books
	    public ActionResult Index()
        {
			var books = new List<Book>();
			//Admin gets all the books
			if(User.IsInRole("Admin"))
			{
				books = db.Books.Include(b => b.Author).Include(b => b.Genre).Include(b => b.AspNetUser).ToList();
			}
			//Users gets all books in the system, and ones added by themselves.
			else if(User.Identity.IsAuthenticated)
			{
				var userId = User.Identity.GetUserId();
				books = db.Books.Where(b => !b.IsDeleted || b.UserId == userId).Include(b => b.Author).Include(b => b.Genre).Include(b => b.AspNetUser).ToList();
			}
			//Unreged users gets all non-deleted books.
			else
			{
				books = db.Books.Where(b => !b.IsDeleted).Include(b => b.Author).Include(b => b.Genre).Include(b => b.AspNetUser).ToList();

			}

			//Shuld have one to show all for admins
			return View(books);
        }

        // GET: Books/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }
		[Authorize]
        // GET: Books/Create
        public ActionResult Create()
        {
			var autorNameList = ControllerMethods.AuthorsFullName();


			ViewBag.AuthorId = new SelectList(autorNameList, "Value", "Text");
			ViewBag.GenreId = new SelectList(db.Genres, "Id", "Name");
			ViewBag.UserId = User.Identity.GetUserId();
			ViewBag.Created = DateTime.Now;

			var book = new Book()
			{
				Created = DateTime.Now,
				UserId = User.Identity.GetUserId()
			};
			return View(book);
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[Authorize]
		[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,AuthorId,GenreId,Title,AlternativeTitle,Part,Description,ISBN,PublishDate,ImagePath,Created,IsDeleted")] Book book)
        {
			if(db.Books.Any(b => b.ISBN == book.ISBN && b.Id != book.Id))
			{
				book.ISBN = "Already in database";
				return View("Create", book);
			}
            if (ModelState.IsValid)
            {
                db.Books.Add(book);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
			//Creates a list with both first and lastname
			var autorNameList = ControllerMethods.AuthorsFullName();
			ViewBag.AuthorId = new SelectList(autorNameList, "Value", "Text");
            ViewBag.GenreId = new SelectList(db.Genres, "Id", "Name", book.GenreId);
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", book.UserId);
            return View(book);
        }

        // GET: Books/Edit/5
		[AuthorizeCreatorAndAdmin]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }

			//Creates a list with both first and lastname
			var autorNameList = ControllerMethods.AuthorsFullName();
			ViewBag.AuthorId = new SelectList(autorNameList, "Value", "Text");
			ViewBag.GenreId = new SelectList(db.Genres, "Id", "Name", book.GenreId);
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", book.UserId);
            return View(book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		[AuthorizeCreatorAndAdmin]
		[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,AuthorId,GenreId,Title,AlternativeTitle,Part,Description,ISBN,PublishDate,ImagePath,Created,IsDeleted")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AuthorId = new SelectList(db.Authors, "Id", "FirstName", book.AuthorId);
            ViewBag.GenreId = new SelectList(db.Genres, "Id", "Name", book.GenreId);
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", book.UserId);
            return View(book);
        }

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}
