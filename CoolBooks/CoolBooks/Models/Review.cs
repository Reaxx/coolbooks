﻿namespace CoolBooks
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;

	public partial class Review
	{
		[Required]
		public int Id { get; set; }
		[Required]
		public int BookId { get; set; }
		[Required]
		public string UserId { get; set; }
		[Required]
		public string Title { get; set; }
		[Required]
		[StringLength(500)]
		public string Text { get; set; }
		[Required]
		[Range(1,10)]
		public Nullable<byte> Rating { get; set; }
		[Required]
		public System.DateTime Created { get; set; }
		public bool IsDeleted { get; set; }

		public virtual Book Book { get; set; }
		public virtual AspNetUser AspNetUser { get; set; }
	}
}
