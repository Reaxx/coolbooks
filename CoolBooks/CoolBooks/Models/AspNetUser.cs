﻿namespace CoolBooks
{
	using System;
	using System.Collections.Generic;

	public partial class AspNetUser
	{
		public AspNetUser()
		{
			this.Books = new HashSet<Book>();
			this.Reviews = new HashSet<Review>();
			this.AspNetUserClaims = new HashSet<AspNetUserClaim>();
			this.AspNetUserLogins = new HashSet<AspNetUserLogin>();
			this.AspNetRoles = new HashSet<AspNetRole>();
		}

		public string Id { get; set; }
		public string Email { get; set; }
		public bool EmailConfirmed { get; set; }
		public string PasswordHash { get; set; }
		public string SecurityStamp { get; set; }
		public string PhoneNumber { get; set; }
		public bool PhoneNumberConfirmed { get; set; }
		public bool TwoFactorEnabled { get; set; }
		public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
		public bool LockoutEnabled { get; set; }
		public int AccessFailedCount { get; set; }
		public string UserName { get; set; }

		public virtual ICollection<Book> Books { get; set; }
		public virtual ICollection<Review> Reviews { get; set; }
		public virtual ICollection<AspNetUserClaim> AspNetUserClaims { get; set; }
		public virtual ICollection<AspNetUserLogin> AspNetUserLogins { get; set; }
		public virtual User User { get; set; }
		public virtual ICollection<AspNetRole> AspNetRoles { get; set; }
	}
}
