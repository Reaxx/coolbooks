﻿namespace CoolBooks
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;

	public partial class Author
	{
		public Author()
		{
			this.Books = new HashSet<Book>();
		}

		[Required]
		public int Id { get; set; }
		[Required]
		public string FirstName { get; set; }
		[Required]
		public string LastName { get; set; }
		[Required]
		public string Description { get; set; }
		[Required]
		public System.DateTime Created { get; set; }
		public bool IsDeleted { get; set; }

		public virtual ICollection<Book> Books { get; set; }
	}
}
