﻿namespace CoolBooks
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;
	using System.Web.Mvc;

	public partial class Book
	{
		public Book()
		{
			this.Reviews = new HashSet<Review>();
		}
		[Required]
		public int Id { get; set; }
		[Required]
		public string UserId { get; set; }
		[Required]
		public int AuthorId { get; set; }
		[Required]
		public int GenreId { get; set; }
		[Required]
		public string Title { get; set; }
		public string AlternativeTitle { get; set; }
		public Nullable<short> Part { get; set; }
		[Required]
		public string Description { get; set; }
		[Remote("IsISBNExists", "Books", AdditionalFields = "Id", ErrorMessage = "ISBN already exists")]
		public string ISBN { get; set; }
		public Nullable<System.DateTime> PublishDate { get; set; }
		[Required]
		public string ImagePath { get; set; }
		[Required]
		public System.DateTime Created { get; set; }
		public bool IsDeleted { get; set; }

	
		public float Rating
		{
			get
			{
				if (Reviews.Count > 0)
				{
					return Reviews.Where(x => !x.IsDeleted).Sum(x => (int)x.Rating) / Reviews.Count(x => !x.IsDeleted);
				}
				else { return 0; }
			}
		}
		
		public int Ratings
		{
			get { return Reviews.Count(x => !x.IsDeleted); }
		}

		public virtual Author Author { get; set; }
		public virtual Genre Genre { get; set; }

		public virtual ICollection<Review> Reviews { get; set; }
		public virtual AspNetUser AspNetUser { get; set; }
	}
}
