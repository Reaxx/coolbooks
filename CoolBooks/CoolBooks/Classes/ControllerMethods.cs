﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoolBooks.Classes
{
	public class ControllerMethods
	{
		public static IQueryable AuthorsFullName()
		{
			CoolBooksEntities db = new CoolBooksEntities();

			return db.Authors.Select(a => new SelectListItem
			{
				Value = a.Id.ToString(),
				Text = a.FirstName + " " + a.LastName
			});
		}
	}
}