﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoolBooks.Classes
{
	public class AuthorizeCreatorAndAdmin : AuthorizeAttribute
	{
		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			var authorized = base.AuthorizeCore(httpContext);
			if (!authorized)
			{
				return false;
			}

			var user = httpContext.User;
			var rd = httpContext.Request.RequestContext.RouteData;
			string controller = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();

			string id = rd.Values["Id"].ToString();
			if (string.IsNullOrEmpty(id))
			{
				return false;
			}
			else if(user.IsInRole("Admin"))
			{
				return true;
			}
			else
			{
				return IsCreatorOfPost(user.Identity.Name, Int32.Parse(id), controller);
			}			
		}

		private bool IsCreatorOfPost(string username, int itemId, string controller)
		{
			var db = new CoolBooksEntities();
			string aspUserName = null;
			if (controller == "Books")
			{ 
				var book = db.Books.Include("AspNetUser").Where(b => b.Id == itemId).First();
				aspUserName = book.AspNetUser.UserName;
			}
			else if (controller == "Reviews")
			{
				var review = db.Reviews.Include("AspNetUser").Where(b => b.Id == itemId).First();
				aspUserName = review.AspNetUser.UserName;
			}

			if (aspUserName == username)
			{
				return true;
			}
			return false;
		}
	}

}