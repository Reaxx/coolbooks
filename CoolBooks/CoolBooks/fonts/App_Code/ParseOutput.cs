﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Parsing for strings in views
/// </summary>
public class ParseOutput
{
	/// <summary>
	/// Truncates and formats a string to a given length
	/// </summary>
	/// <param name="input">String to truncatre</param>
	/// <param name="length">Length to truncate to</param>
	/// <returns></returns>
	public static string Truncate(string input, int length)
	{ 
		if (input.Count() <= length)
		{
			return input;
		}
		else
		{
			return input.Substring(0, 200) + "..";				
		}
	}
}